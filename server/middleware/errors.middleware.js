const constants = require('../constants/errors');
const ErrorBase = require('../errors/error-base');
const httpStatusCode = require('http-status-codes');

module.exports = {
    notFoundError: (req, res, next) => {
        next(
            new ErrorBase(
                constants.NOT_FOUND_EXCEPTION_MESSAGE,
                httpStatusCode.NOT_FOUND,
                req.originalUrl
                ));
    },
    badRequestError: (req, res, next) => {
        next(
            new ErrorBase(
                constants.BAD_REQUEST_MESSAGE,
                httpStatusCode.FORBIDDEN,
                req.originalUrl
                ));
    },

    internalError: (err, req, res, next) => { // eslint-disable-line
        const status = err.status || constants.INTERNAL_ERROR_MESSAGE;
        res.status(status);
        res.json({
            status,
            error: err.message,
            description: err.description || err.errors || {},
        });
    },
};
