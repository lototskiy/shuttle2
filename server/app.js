const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const endpoints = require('./constants/endpoints');
const errors = require('./middleware/errors.middleware');

// Routes includes
const indexRouter = require('./routes/index.router');
const petsRouter = require('./routes/pets.router');
const donationRouter = require('./routes/donation.router');
const newsRouter = require('./routes/news.router');
const userRouter = require('./routes/user.router');

const app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Routes declaration
app.use(endpoints.rootEndpoint, indexRouter);
app.use(endpoints.petsEndpoint, petsRouter);
app.use(endpoints.donationEndpoint, donationRouter);
app.use(endpoints.newsEndpoint, newsRouter);
app.use(endpoints.userEndpoint, userRouter);
// Errors handler
app.use(errors.notFoundError);
app.use(errors.badRequestError);
app.use(errors.internalError);


module.exports = app;
