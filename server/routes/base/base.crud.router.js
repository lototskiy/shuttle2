const responses = require('../../constants/responses');
const endpoints = require('../../constants/endpoints');

class RouterProvider {
    constructor(modelName, router) {
        this._modelName = modelName;
        this.router = router;
        this.router.get(endpoints.rootEndpoint, this.list.bind(this));
        this.router.get(endpoints.getByIdEndpoint, this.show.bind(this));
        this.router.post(endpoints.rootEndpoint, this.create.bind(this));
        this.router.put(endpoints.getByIdEndpoint, this.update.bind(this));
        this.router.delete(endpoints.getByIdEndpoint, this.remove.bind(this));
    }

    list(req, res) {
        res.json(responses.ok());
    }

    show(req, res) {
        res.json(responses.ok());
    }

    create(req, res) {
        res.json(responses.ok());
    }

    update(req, res) {
        res.json(responses.ok());
    }

    remove(req, res) {
        res.json(responses.ok());
    }
}

module.exports = RouterProvider;
