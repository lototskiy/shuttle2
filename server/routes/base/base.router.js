const endpoints = require('../../constants/endpoints');
const config = require('../../config/server.config');
const httpStatusCodes = require('http-status-codes');


class RouterProvider {
    constructor(modelName, router) {
        this._modelName = modelName;
        this.router = router;
        this.router.get(endpoints.rootEndpoint, this.list.bind(this));
        this.router.get(endpoints.getByIdEndpoint, this.show.bind(this));
        this.router.post(endpoints.rootEndpoint, this.create.bind(this));
        this.router.put(endpoints.getByIdEndpoint, this.update.bind(this));
        this.router.delete(endpoints.getByIdEndpoint, this.remove.bind(this));
    }

    list(req, res, next) {
        const page = req.query.pageNumber || config.FIRST_PAGE;
        const perPage = req.query.itemsPerPage || config.DEFAULT_ITEMS_PER_PAGE;
        const queryObj = Object.assign({}, req.query);
        queryObj.headers = req.headers;

        this._modelName
            .find(queryObj)
            .then(data => res.json(this.__pagination(data, page, perPage)))
            .catch(error => next(error));
    }

    show(req, res, next) {
        return this._modelName
            .findById(req.params.id)
            .then(data => res.json(data))
            .catch(error => next(error));
    }

    create(req, res, next) {
        return this._modelName
        .create(Object.assign({}, req.body))
            .then(data => res.json(data))
            .catch(error => next(error));
    }

    update(req, res, next) {
        this._modelName
        .findById(req.params.id)
        .then(data => Object.assign(
                {}, data.toObject(), req.body, { UpdateDate: Date.now() }))
        .then(data => this._modelName.update(data))
        .then(data => res.json({
            status: data.nModified ? httpStatusCodes.OK : httpStatusCodes.NOT_MODIFIED,
        }))
        .catch(error => next(error));
    }

    remove(req, res, next) {
        return this._modelName
            .remove(req.params.id)
            .then(data => res.json({
                status: data.nModified ? httpStatusCodes.OK : httpStatusCodes.NOT_MODIFIED,
            }))
            .catch(error => next(error));
    }

    __pagination(data, page, perPage) {
        const Data = data.slice((page - 1) * perPage, page * perPage);
        const TotalCount = data.length;
        return { Data, TotalCount };
    }
}

module.exports = RouterProvider;
