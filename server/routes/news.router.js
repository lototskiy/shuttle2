const express = require('express');
const router = express.Router(); //eslint-disable-line

const newsServe = require('../services/news.service');
const RouterProvider = require('./base/base.router');

const NewsProvider = new RouterProvider(newsServe, router);
module.exports = NewsProvider.router;
