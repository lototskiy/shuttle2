const express = require('express');
const router = express.Router(); //eslint-disable-line

const petsService = require('../services/pets.service');
const RouterProvider = require('./base/base.router');

const PetsProvider = new RouterProvider(petsService, router);
module.exports = PetsProvider.router;
