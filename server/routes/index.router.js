const express = require('express');
const endPoints = require('../constants/endpoints');
const statusCode = require('http-status-codes');
const responses = require('../constants/responses');
const errorTest = require('../middleware/errors.middleware.js');

const router = express.Router(); //eslint-disable-line

router.get(endPoints.pingEndpoint, (req, res) => {
    res.status(statusCode.OK);
    res.json(responses.ok());
});
router.get(endPoints.badEndpoint, (req, res, next) => errorTest.badRequestError(req, res, next));

module.exports = router;
