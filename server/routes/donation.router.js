const express = require('express');
const router = express.Router(); //eslint-disable-line

const donationService = require('../services/donation.service');
const RouterProvider = require('./base/base.crud.router');

const DonationProvider = new RouterProvider(donationService, router);
module.exports = DonationProvider.router;
