const ErrorBase = require('../../errors/error-base');
const ObjectId = require('mongoose').Types.ObjectId;
const Constants = require('../../constants/errors');
const httpStatusCode = require('http-status-codes');

class ModelProvider {
    constructor(modelName, options = {}) {
        this._modelName = modelName;
        this._options = options;
    }

    create(model) {
        return new this._modelName(model)
            .save()
            .then(data => data);
    }

    findByCustomId(id) {
        return this._modelName
            .findById(id)
            .then(data => {
                if (data === null) {
                    throw new ErrorBase(
                        Constants.NOT_FOUND_EXCEPTION_MESSAGE,
                        httpStatusCode.NOT_FOUND);
                }
                return data;
            });
    }

    findById(id) {
        if (!ObjectId.isValid(id)) {
            return Promise.reject(
                new ErrorBase(
                    Constants.BAD_REQUEST_MESSAGE,
                    httpStatusCode.BAD_REQUEST)
            );
        }

        const populate = (this._options.populate || []).join(' ');

        return this._modelName
            .findById(id)
            .populate(populate)
            .then(data => {
                if (data === null) {
                    throw new ErrorBase(
                        Constants.NOT_FOUND_EXCEPTION_MESSAGE,
                        httpStatusCode.NOT_FOUND);
                }
                return data;
            });
    }

    find() {
        const searchingCriteria = { IsActive: true };

        return this._modelName
            .find(searchingCriteria)
            .exec();
    }

    update(model) {
        const _id = model._id;

        return this.findById(_id)
            .then(() => this._modelName.update({ _id }, { $set: model }));
    }

    remove(id) {
        return this.findById(id)
            .then(() => this._modelName.update(
                { _id: id },
                { $set: { IsActive: false },
            }));
    }
}

module.exports = ModelProvider;
