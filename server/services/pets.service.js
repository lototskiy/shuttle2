const Pets = require('../models/pets.model');
const BaseProvider = require('./base/data.base.service');

class PetsProvider extends BaseProvider {
}

module.exports = new PetsProvider(Pets, {});
