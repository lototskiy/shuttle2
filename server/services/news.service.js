const News = require('../models/news.model');
const BaseProvider = require('./base/data.base.service');

class NewsProvider extends BaseProvider {
}

module.exports = new NewsProvider(News, {});
