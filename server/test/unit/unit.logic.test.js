const logicExample = require('../example/logic.example');

const expect = require('chai').expect;

describe('Verify the getFileExtension() function', () => {
    it('Correct file name. Should return file name', () => {
        const filename = 'filename.txt';
        const extension = filename.split('.')[1];
        expect(logicExample.getFileExtension(filename)).to.equal(extension);
    });
    it('Incorrect file name. Should return Undefined', () => {
        expect(logicExample.getFileExtension('1,2,3')).to.be.undefined;
    });
});

describe('Verify the maxNumberArray() function', () => {
    it('Array. Should return max number', () => {
        expect(logicExample.maxNumberArray([1, 2, 3])).to.equal(3);
    });
    it('Array in the in reverse order. Should return max number', () => {
        expect(logicExample.maxNumberArray([4, 6, 7, 1])).to.equal(7);
    });
    it('Empty Array. Should return Infinity', () => {
        expect(logicExample.maxNumberArray([])).to.be.Infinity;
    });
});

describe('Verify the minNumberArray() function', () => {
    it('Array. Should return min number', () => {
        expect(logicExample.minNumberArray([1, 2, 3])).to.equal(1);
    });
    it('Empty Array. Should return Infinity', () => {
        expect(logicExample.minNumberArray([])).to.be.Infinity;
    });
    it('Array in the in reverse order. Should return min number', () => {
        expect(logicExample.minNumberArray([4, 6, 7, 1])).to.equal(1);
    });
});

describe('Verify the isString() function', () => {
    it('Correct string. Should return True', () => {
        expect(logicExample.isString('Kate')).to.equal(true);
    });
    it('Send object. Should return False', () => {
        expect(logicExample.isString(Object)).to.equal(false);
    });
});

describe('Verify the isEmail() function', () => {
    it('Correct email. Should return True', () => {
        const email = 'kategrek@gmail.com';
        expect(logicExample.isEmail(email)).to.equal(true);
    });
    it('Incorrect email. Should return False', () => {
        expect(logicExample.isEmail('kategrekgmail.com')).to.equal(false);
    });
});

describe('Verify the isUrl() function', () => {
    it('Correct URL. Should return URL', () => {
        expect(logicExample.isUrl('http://javascript.ru')).to.equal(true);
    });
    it('Incorrect URL. Should return False', () => {
        expect(logicExample.isUrl('Object')).to.equal(false);
    });
});

describe('Verify the getDate() function', () => {
    it('Current date. Should return date', () => {
        expect(logicExample.getDate()).to.be.date;
    });
});
