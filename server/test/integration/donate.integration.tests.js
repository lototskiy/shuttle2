const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app');
const httpStatusCodes = require('http-status-codes');
const expect = chai.expect; //eslint-disable-line
const should = chai.should(); //eslint-disable-line

chai.use(chaiHttp);
const donateRootRoute = '/donation';
const existingDonateRoute = '/donation/1';

describe.skip('Donate API tests', () => {
    it('Get all donations', (done) => {
        chai.request(server)
        .get(donateRootRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Get existing donation by Id', (done) => {
        chai.request(server)
        .get(existingDonateRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Create new donation', (done) => {
        chai.request(server)
        .post(donateRootRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Update existing donation by Id', (done) => {
        chai.request(server)
        .put(existingDonateRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Delete existing donation by Id', (done) => {
        chai.request(server)
        .delete(existingDonateRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });
});
