const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app');
const httpStatusCodes = require('http-status-codes');
const expect = chai.expect; //eslint-disable-line
const should = chai.should(); //eslint-disable-line

chai.use(chaiHttp);
const petsRootRoute = '/pets';
const existingPetRoute = '/pets/1';

describe.skip('Pets API tests', () => {
    it('Get all pets', (done) => {
        chai.request(server)
        .get(petsRootRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Get existing pet by Id', (done) => {
        chai.request(server)
        .get(existingPetRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Create new pet', (done) => {
        chai.request(server)
        .post(petsRootRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Update existing pet by Id', (done) => {
        chai.request(server)
        .put(existingPetRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Delete existing pet by Id', (done) => {
        chai.request(server)
        .delete(existingPetRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });
});
