module.exports = {
    // This function return string with name of the file extension Should return
    // boolean
    getFileExtension: (filename) => {
        const result = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
        return result;
    },

    // Check max number in array of numbers Should return number
    maxNumberArray: (array) => {
        const result = Math.max.apply(Math, array);
        return result;
    },

    // Check min number in array of numbers Should return number
    minNumberArray: (array) => {
        const result = Math.min.apply(Math, array);
        return result;
    },

    // Check is it a string Required in next functions (isEmail, isUrl) Should
    // return boolean
    isString: (obj) => {
        const result = typeof(obj) === 'string';
        return result;
    },

    // Check is it an Email Should return boolean
    isEmail: (obj) => {
        let result = false;
        if (typeof obj === 'string') {
            result = (/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/ig).test(obj);
            return result;
        }
        return result;
    },

    // Check is it URL Should return boolean
    isUrl: (obj) => {
        let result = false;
        if (typeof obj === 'string') {
            const re = /^(ftp|http|https):\/\/[^ "]+$/;
            result = re.test(obj);
            return result;
        }
        return result;
    },

    // Return Current date, with support polyfil for old browsers Should return date
    getDate: () => {
        if (!Date.now) {
            Date.now = function now() {
                return new Date().getTime();
            };
        } else {
            return new Date();
        }
    },
};
