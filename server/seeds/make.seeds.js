const User = require('../models/user.model');
const Pet = require('../models/pets.model');
const News = require('../models/news.model');
const config = require('../config/mongo.config.js');
const ProviderConstructor = require('../services/base/connection.service');

const Provider = new ProviderConstructor(config);

let userPromiseArray = [];
let petPromiseArray = [];
let newsPromiseArray = [];
let resultPromiseArray = [];

Provider.openConnection();

const seedUsers = [
    new User({
        UserName: 'SuperIgor',
        Email: 'superuser@superigor.com',
        Role: 'User',
        Phone: '+380679999999',
        Skype: 'SuperIgor',
        Description: 'Best User ever',
    }),
    new User({
        UserName: 'SuperLisa',
        Email: 'supermanager@superlisa.com',
        Role: 'Manager',
        Phone: '+380679999999',
        Skype: 'SuperLisa',
        Description: 'Best Manager ever',
    }),
    new User({
        UserName: 'SuperDima',
        Email: 'superadmin@superdima.com',
        Role: 'Administrator',
        Phone: '+380679999999',
        Skype: 'SuperAdmin',
        Description: 'Best Admin ever',
    })];

const seedPet = [
    new Pet({
        PetName: 'Bobik',
        Description: 'Best Dog Ever',
        PetCategoryId: 1,
    }),
    new Pet({
        PetName: 'President',
        Description: 'Best Snake Ever',
        PetCategoryId: 2,
    }),
    new Pet({
        PetName: 'Shura',
        Description: 'Best cat Ever',
        PetCategoryId: 3,
    })];

const seedNews = [
    new News({
        Description: 'First glorious news',
        Title: 'We have a new dog!',
        CategoryId: 1,
    }),
    new News({
        Description: 'Second glorious news',
        Title: 'We have a new snake!',
        CategoryId: 2,
    }),
    new News({
        Description: 'Third glorious news',
        Title: 'We have a new cat!',
        CategoryId: 3,
    })];

function createPromise(model) { // sample async action
    return new Promise((resolve) => {
        model.save(() => resolve());
    }).catch(err => console.log(err));
}

userPromiseArray = seedUsers.map(createPromise);
petPromiseArray = seedPet.map(createPromise);
newsPromiseArray = seedNews.map(createPromise);

resultPromiseArray = userPromiseArray.concat(petPromiseArray).concat(newsPromiseArray);

const finishedPromise = Promise.all(resultPromiseArray);

finishedPromise.then(() => {
    console.log('Seeds Created');
    Provider.closeConnection();
});

finishedPromise.catch(err => console.log(err));
