const mongoose = require('mongoose');
require('mongoose-schema-extend');
const SchemaBase = require('./base/base.model');
const modelNames = require('../constants/model.names');
const Promise = require('bluebird');

mongoose.Promise = Promise;

const petSchema = SchemaBase.extend({
    PetName: {
        type: String,
        required: true,
    },
    Description: {
        type: String,
        required: false,
    },
    PetTag: {
        type: String,
        required: false,
    },
    Image: {
        type: String,
        required: false,
    },
    PetCategoryId: {
        type: Number,
        required: true,
    },
});

const Pet = mongoose.model(modelNames.pets, petSchema);

module.exports = Pet;
