const mongoose = require('mongoose');
require('mongoose-schema-extend');
const SchemaBase = require('./base/base.model');
const modelNames = require('../constants/model.names');
const Promise = require('bluebird');

mongoose.Promise = Promise;

const userSchema = SchemaBase.extend({
    UserName: {
        type: String,
        required: true,
    },
    Email: {
        type: String,
        required: true,
    },
    Phone: {
        type: String,
        required: false,
    },
    Skype: {
        type: String,
        required: false,
    },
    Description: {
        type: String,
        required: false,
    },
    Image: {
        type: String,
        required: false,
    },
    Role: {
        type: String,
        required: true,
        enum: ['User', 'Manager', 'Administrator'],
        default: 'User',
    },
});

const User = mongoose.model(modelNames.user, userSchema);

module.exports = User;
