class ErrorBase extends Error {

    constructor(message, status = 500, description = '') {
        super(message);

        this.message = message;
        this.status = status;
        this.description = description;
    }
}

module.exports = ErrorBase;
